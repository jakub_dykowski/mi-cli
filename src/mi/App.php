<?php
namespace mi;

use Exception;
use InvalidArgumentException;
use SplFileInfo;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class App {

    private static $fileDiffPrinted = false;

    /**
     * @param string $message
     * @param null $args,...
     */
    public static function error($message, $args = null) {
        vprintf("ERROR: $message" . PHP_EOL, array_slice(func_get_args(), 1));
        exit(1);
    }

    /**
     * @param string $message
     * @param null $args,...
     */
    public static function debug($message, $args = null) {
        if (DEBUG)
            vprintf("DEBUG: $message" . PHP_EOL, array_slice(func_get_args(), 1));
    }

    /**
     * @param string $message
     * @param null $args,...
     */
    public static function info($message, $args = null) {
        if (DEBUG)
            vprintf("INFO: $message" . PHP_EOL, array_slice(func_get_args(), 1));
    }

    public static function println($message, $args = null) {
        vprintf($message . PHP_EOL, array_slice(func_get_args(), 1));
    }

    public static function printReturn($message, $args = null) {
        vprintf("\r$message", array_slice(func_get_args(), 1));
    }

    public static function fileAddedInfo(SplFileInfo $file, $path) {
        self::fileDiffInfo();
        printf(" A %s" . PHP_EOL, $path);
    }

    public static function fileChangedInfo(SplFileInfo $file, $path) {
        self::fileDiffInfo();
        printf(" M %s" . PHP_EOL, $path);
    }

    public static function prettySize($bytes) {
        if ($bytes < 1024) {
            return sprintf("%dB", $bytes);
        } elseif ($bytes < 1024 * 1024) {
            return sprintf("%dKiB", $bytes / 1024);
        } else if ($bytes < 1024 * 1024 * 1024) {
            return sprintf("%dMiB", $bytes / 1024 / 1024);
        } else if ($bytes < 1024 * 1024 * 1024 * 1024) {
            return sprintf("%dGiB", $bytes / 1024 / 1024 / 1024);
        } else {
            return sprintf("%dTiB", $bytes / 1024 / 1024 / 1024 / 1024);
        }
    }

    public static function helpInfo() {
        App::println("usage:");
        App::println("  init");
        App::println("  deploy [diff]");
        App::println("    diff - (default) upload only changed or new files, except ignored ones");
        App::println("  upload [<file1>[, <file2> ...]");
        App::println("     upload selected file(s)");
        App::println("  maintenance [true|false]");
        App::println("    diff - (default) upload only changed or new files");
        App::println("  version");
        App::println("      displays version number");
        App::println("  help");
        App::println("      displays this help message");
    }

    /**
     * @param $configFileName
     * @return null|string
     * @throws Exception when this project is nested inside another one
     */
    public static function discoverProjectRoot($configFileName) {
        $projectDir = getcwd();
        while (!file_exists("$projectDir/$configFileName")) {
            $projectDir = dirname($projectDir);
            if ($projectDir === DIRECTORY_SEPARATOR || preg_match("/^[A-Z]:\\\\$/", $projectDir))
                return null; // ostatni katalog w hierarchii sprawdzony, nie ma więcej katalogów
        }

        // sprawdź czy obecny projekt nie jest przypadkiem zagnieżdżony w innym
        $parentProjectDir = dirname($projectDir);
        if ($parentProjectDir === $projectDir)
            return $projectDir; // znaczy że projectDir jest w głównym katalogu
        while (!file_exists("$parentProjectDir/$configFileName")) {
            $parentProjectDir = dirname($parentProjectDir);
            if ($parentProjectDir === DIRECTORY_SEPARATOR || preg_match("/^[A-Z]:\\\\$/", $parentProjectDir))
                return $projectDir; // dobrze, nasz projekt nie jest zagnieżdżony w innym
        }

        throw new Exception("Current project($projectDir) is nested inside another($parentProjectDir)");
    }

    public static function getProjectRelativePath($absolutePath, $projectRoot) {
        // +2 bo '/' oraz conajmniej jeden znak pliku/folderu
        if (mb_strlen($absolutePath) < (mb_strlen($projectRoot) + 2))
            throw new InvalidArgumentException("invalid absolutePath($absolutePath) for projectRoot($projectRoot)");
        return substr($absolutePath, mb_strlen($projectRoot) + 1);
    }

    /**
     * @param string $projectDir
     * @param Settings $settings
     * @return FileSystemInfo
     */
    public static function getLocalFileSystemInfo($projectDir, Settings $settings) {
        // check the os we are running on and select proper implementation
        if (mb_strtoupper(mb_substr(PHP_OS, 0, 3)) === 'WIN') {
            $localFs = new LocalPhpFileSystemInfo($projectDir);
        } else {
            $localFs = new ShellFileSystemInfo(new LocalEnvironment($projectDir), $settings->ignore);
        }
        return $localFs;
    }

    private static function fileDiffInfo() {
        if (!self::$fileDiffPrinted) {
            App::println("Production diff:");
            self::$fileDiffPrinted = true;
        }
    }
}