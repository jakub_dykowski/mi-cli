<?php
namespace mi;

use Exception;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class Settings {

    /**
     * Oznacza wersję narzędzia która zapisała ten plik.
     * Jeśli narzędzie odczytuje ustawienia i wersja w ustawieniach jest nowsza niż samo narzędzie, narzędzie nie
     * powinno używać tych ustawień.
     *
     * @var SemVer
     */
    public $version;

    /**
     * @var Host
     */
    public $server;

    /**
     * @var array
     */
    public $ignore = [];

    private $filename;

    public function __construct($filename) {
        $this->filename = $filename;
        $this->server = new Host();
    }

    /**
     * @return bool
     */
    public function exists() {
        return file_exists($this->filename);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function load() {
        if (!$this->exists())
            return false;

        $rawSettings = json_decode(file_get_contents($this->filename));
        if (json_last_error() > 0)
            throw new Exception("invalid json in config file: $this->filename code: " . json_last_error());
        foreach ($rawSettings as $name => $value) {
            switch ($name) {
                case "server":
                    // dla server kopiujemy ręcznie, bo chcemy zachować obiekt Host
                    foreach ($value as $propertyName => $propertyValue)
                        $this->server->$propertyName = $propertyValue;
                    break;
                case "version":
                    $this->version = new SemVer($value);
                    break;
                default:
                    $this->$name = $value;
            }
        }

        // chcemy uniknąć odczytywania ustawień przez starą aplikację
        $settingsComparedToScript = $this->version->compareTo(new SemVer(VERSION));
        if ($settingsComparedToScript > 0) {
            App::error("settings version (%s) newer than script (%s)", $this->version, VERSION);
            exit(1);
        } else {
            // if settings has lower version, and update causes breaking changes, upgrade settings and their version
        }

        return true;
    }

    public function save() {
        App::debug("save settings: %s", $this->filename);

        // we need to copy contents of $this, and change version from object to string because json_encode doesn't
        // know how to do that
        $settings = clone $this;
        $settings->version = (string)$this->version;

        // JSON_UNESCAPED_SLASHES jest po to by nie escapować '/', jest to niepotrzebne i nieczytelne
        file_put_contents($this->filename, json_encode($settings, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES), LOCK_EX);
    }

    public function getFile() {
        return $this->filename;
    }
}