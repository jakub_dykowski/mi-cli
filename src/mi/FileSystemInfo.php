<?php
namespace mi;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
interface FileSystemInfo {

    /**
     * @param string $file
     * @return boolean
     */
    public function fileExists($file);

    /**
     * @param string $dir
     * @return boolean
     */
    public function dirExists($dir);

    /**
     * @param string $file
     * @return string|null
     */
    public function getChecksum($file);
}