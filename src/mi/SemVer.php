<?php
namespace mi;

use InvalidArgumentException;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class SemVer {

    const SORT = [self::class, "compare"];

    private $major;
    private $minor;
    private $patch;

    public function __construct(string $version) {
        if (!preg_match("/^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$/", $version))
            throw new InvalidArgumentException("invalid semver: $version");

        list($major, $minor, $patch) = explode(".", $version);
        $this->major = (int)$major;
        $this->minor = (int)$minor;
        $this->patch = (int)$patch;
    }

    public function getMajor(): int {
        return $this->major;
    }

    public function getMinor(): int {
        return $this->minor;
    }

    public function getPatch(): int {
        return $this->patch;
    }

    public function compareTo(SemVer $other): int {
        if ("$this" === "$other") {
            return 0;
        } else {
            $list = [$this, $other];
            natsort($list);
            $list = array_values($list); // natstort keeps key value association, we need to reindex
            return $list[0] === $this ? -1 : 1;
        }
    }

    public static function compare(SemVer $a, SemVer $b): int {
        return $a->compareTo($b);
    }

    function __toString() {
        return "$this->major.$this->minor.$this->patch";
    }
}