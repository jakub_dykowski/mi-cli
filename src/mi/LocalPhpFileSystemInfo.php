<?php
namespace mi;

use InvalidArgumentException;

/**
 * This implementation uses plain PHP to get file system information.
 * It doesn't hurt if we don't exclude ignored files here, not yet at least.
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class LocalPhpFileSystemInfo implements FileSystemInfo {

    private $path;

    /**
     * @param string $path
     */
    public function __construct($path) {
        if (!is_string($path))
            throw new InvalidArgumentException("path is not a string");
        if (!is_dir($path))
            throw new InvalidArgumentException("path is not a directory: $path");

        $this->path = rtrim($path, "/");
    }

    /**
     * @param string $file
     * @return boolean
     */
    public function fileExists($file) {
        return is_file("$this->path/$file");
    }

    /**
     * @param string $dir
     * @return boolean
     */
    public function dirExists($dir) {
        return is_dir("$this->path/$dir");
    }

    /**
     * @param string $file
     * @return string|null
     */
    public function getChecksum($file) {
        return md5_file("$this->path/$file");
    }
}