<?php
namespace mi;

use Exception;
use InvalidArgumentException;
use phpseclib\Net\SCP;
use phpseclib\Net\SSH2;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class Host {

    public $host;
    public $dir;
    public $username;
    public $password;

    /**
     * @var SSH2|null
     */
    private $ssh2;

    /**
     * @var SCP|null
     */
    private $scp;

    public function isConnected() {
        return $this->ssh2 !== null && $this->ssh2->isAuthenticated();
    }

    /**
     * return bool
     */
    public function connect() {
        App::debug("ssh connect... %s@%s:", $this->username, $this->host);
        $this->ssh2 = new SSH2($this->host);
        $this->scp = new SCP($this->ssh2);

        if ($this->ssh2->login($this->username, $this->password)) {
            App::debug("authenticated: %s", $this->getFullName());
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $file
     * @param string $to
     * @return bool
     */
    public function uploadFile($file, $to) {
        if (!file_exists($file))
            throw new InvalidArgumentException("file doesn't exist: $file");

        $success = $this->scp->put($to, $file, SCP::SOURCE_LOCAL_FILE);
        if ($success) {
            App::debug("uploaded file %s to %s", $file, $to);
        } else {
            App::debug("failed uploading file %s to %s", $file, $to);
        }
        return $success;
    }

    /**
     * @param string $command
     * @return string
     * @throws Exception
     */
    public function exec($command) {
        App::debug("%s %s...", $this, $command);

        $output = $this->ssh2->exec("$command; echo \"\n$?\";");

        // cała magia polega na doklejeniu do końca output \n i kodu, a potem odczytaniu tego
        $delimeterPosition = strrpos($output, "\n", -2);
        $exitCode = intval(trim(substr($output, $delimeterPosition)));
        $trueOutput = substr($output, 0, $delimeterPosition);
        if ($exitCode > 0)
            throw new Exception("exit code $exitCode from: $this $command saying: $trueOutput", $exitCode);

        App::debug(" OK: %s", $trueOutput == '' ? "<empty>" . PHP_EOL : $trueOutput);

        return $trueOutput;
    }

    public function getFullName() {
        return "$this->username@$this->host";
    }

    function __toString() {
        return sprintf("<%s %s>", get_class(), $this->getFullName());
    }
}