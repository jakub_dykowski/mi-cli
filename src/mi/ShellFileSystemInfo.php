<?php
namespace mi;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class ShellFileSystemInfo implements FileSystemInfo {

    private $checksumCache = null;
    private $justDirCache = null;
    private $env;
    private $ignoreList;

    public function __construct(Environment $env, array $ignore = null) {
        // potrzebuje env które już jest ustawione na katalog projektu
        $this->env = $env;
        $this->ignoreList = $ignore !== null ? $ignore : [];
    }

    public function fileExists($file) {
        return $this->getChecksum($file) !== null;
    }

    public function dirExists($dir) {
        if ($this->justDirCache === null) {
            // leniwe ładowanie
            App::debug("%s fetching dir list...", $this->env);
            $this->justDirCache = [];
            $dirsOutput = $this->env->exec(self::buildFindDirsCommand($this->ignoreList));
            $line = strtok($dirsOutput, PHP_EOL);
            do {
                $path = mb_substr($line, 2); // pozbywamy się początkowego './'
                $this->justDirCache[$path] = null;
            } while ((($line = strtok(PHP_EOL)) !== false));
            App::debug("%s dir list fetched", $this->env);
        }

        return array_key_exists($dir, $this->justDirCache);
    }

    public function getChecksum($file) {
        // leniwie pobieramy cały katalog projektu, rekursuwnie
        if ($this->checksumCache === null) {
            App::debug("%s calculating checksums...", $this->env);
            $this->checksumCache = [];
            $checksumsOutput = $this->env->exec(self::buildFindFilesCommand($this->ignoreList) . " | xargs -0 md5sum");
            $line = strtok($checksumsOutput, PHP_EOL);
            do {
                list($checksum, $path) = preg_split("/\s+/", $line, 2);
                $path = mb_substr($path, 2); // pozbywamy się początkowego './'
                $this->checksumCache[$path] = $checksum;
            } while ((($line = strtok(PHP_EOL)) !== false));
            App::debug("%s checksums calculated", $this->env);
        }

        return array_key_exists($file, $this->checksumCache) ? $this->checksumCache[$file] : null;
    }

    public function clearCache() {
        $this->checksumCache = null;
        $this->justDirCache = null;
    }

    private static function buildFindDirsCommand(array $ignoreList) {
        $options = "";
        foreach ($ignoreList as $path) {
            if ($options !== "")
                $options .= " -o";
            $escapedPath = escapeshellarg("./$path");
            $options .= " -path $escapedPath -prune";
        }
        // why its '-not'? (not '-o') I don't know, but it works
        return "find -type d -not \\($options \\)";
    }

    private static function buildFindFilesCommand(array $ignoreList) {
        $options = "";
        foreach ($ignoreList as $path) {
            if ($options !== "")
                $options .= " -o";
            $escapedPath = escapeshellarg("./$path");
            $options .= " -path $escapedPath -prune";
        }
        // why its '-o'? (not '-not') I don't know, but it works
        return "find \\($options \\) -o -type f -print0";
    }
}