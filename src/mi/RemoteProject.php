<?php
namespace mi;

use Exception;
use InvalidArgumentException;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class RemoteProject extends Environment {

    /**
     * @var ShellFileSystemInfo
     */
    private $fs;

    /**
     * @var Host
     */
    private $server;

    public function __construct(Host $server, array $ignoreList) {
        $this->server = $server;
        $this->fs = new ShellFileSystemInfo($this, $ignoreList);
    }

    public function sendFile($absolutePath, $to) {
        $realTo = "{$this->server->dir}/$to";
        return $this->server->uploadFile($absolutePath, $realTo);
    }

    /**
     * Checks whether file actually exists.
     * Does NOT use caching. Checking multiple files is very slow.
     * @param $file
     * @return bool
     */
    public function fileExists($file) {
        $output = $this->exec("[ ! -f '$file' ] || echo exists");
        return $output === 'exists';
    }

    public function isFileEqual($file, FileSystemInfo $sourceFs) {
        return $sourceFs->getChecksum($file) == $this->fs->getChecksum($file);
    }

    public function getFileSystemInfo() {
        return $this->fs;
    }

    /**
     * @param bool $maintenance
     */
    public function setMaintenance($maintenance) {
        if (!is_bool($maintenance))
            throw new InvalidArgumentException("not bool");
        if ($maintenance) {
            $this->exec("touch " . MAINTENANCE_FILE_NAME);
        } else {
            $this->exec("rm " . MAINTENANCE_FILE_NAME);
        }
    }

    /**
     * @param bool $lock
     * @return bool success
     * @throws Exception
     */
    public function setLock($lock) {
        if (!is_bool($lock))
            throw new InvalidArgumentException("not bool");
        $lockFile = ".mi-cli.lock";
        if ($lock) {
            try {
                $this->exec("set -o noclobber ; > '$lockFile'");
            } catch (Exception $e) {
                if ($e->getCode() == 1)
                    return false; // plik już istnieje
                else
                    throw $e;
            }
        } else {
            $this->exec("rm $lockFile");
        }
        return true;
    }

    /**
     * return @bool
     */
    public function isMaintenance() {
        return $this->fs->fileExists(MAINTENANCE_FILE_NAME);
    }

    /**
     * @param string $command
     * @return string
     */
    public function exec($command) {
        $output = $this->server->exec("cd {$this->server->dir} && {$command}");
        if (strlen($output) > 0 && substr($output, -1))
            return substr($output, 0, strlen($output) - 1);
        else
            return $output;
    }
}