<?php
namespace mi;

use Exception;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
final class ShellUtils {

    private function __construct() { }

    public static function getFileModificationTimeCommand($file) {
        return "stat -c %Y '$file'";
    }

    public static function exec($command) {
        exec($command, $resultLines, $returnCode);
        if ($returnCode !== 0)
            throw new Exception("exit code($returnCode): " . implode(PHP_EOL, $resultLines), $returnCode);
        return implode(PHP_EOL, $resultLines);
    }

    /**
     * @param string $message
     * @param bool $defaultAnswer
     * @return null
     */
    public static function confirm($message, $defaultAnswer = false) {
        printf("%s %s ", $message, $defaultAnswer ? "[Y/n]" : "[y/N]");
        $line = fgets(STDIN);
        $answer = trim($line);
        return $defaultAnswer ? ($answer == '' || $answer == 'y') : ($answer == 'y');
    }

    /**
     * @param null $question
     * @param null $defaultAnswerValue
     * @param null $defaultAnswerLabel
     * @return null|string next line or null if blank
     */
    public static function readLine($question = null, $defaultAnswerValue = null, $defaultAnswerLabel = null) {
        if ($question !== null)
            printf("%s%s ", $question, $defaultAnswerValue !== null ? " [$defaultAnswerLabel]" : "");
        $line = trim(fgets(STDIN));
        return $line === "" ? $defaultAnswerValue : $line;
    }
}