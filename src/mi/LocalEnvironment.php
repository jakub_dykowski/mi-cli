<?php
namespace mi;

use Exception;
use InvalidArgumentException;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class LocalEnvironment extends Environment {

    private $cwdPath;

    public function __construct($cwdPath) {
        if (!is_string($cwdPath) || $cwdPath === '')
            throw new InvalidArgumentException("invalid cwdPath: $cwdPath");
        // musimy znać CWD, by z niego uruchomić metody, tak jak robimy to w RemoteProject
        $this->cwdPath = $cwdPath;
    }

    /**
     * @param string $command
     * @return string
     * @throws Exception
     */
    public function exec($command) {
        return ShellUtils::exec("cd $this->cwdPath && $command");
    }
}