<?php
namespace mi;

use Exception;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
abstract class Environment {

    /**
     * @param string $command
     * @return string
     * @throws Exception
     */
    public abstract function exec($command);

    public function __toString() {
        return "<" . get_class($this) . ">";
    }
}