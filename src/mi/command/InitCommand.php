<?php
namespace mi\command;

use InvalidArgumentException;
use mi\SemVer;
use mi\Settings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class InitCommand extends Command {

    /** @var Settings */
    private $settings;

    public function __construct(Settings $settings) {
        parent::__construct();
        $this->settings = $settings;
    }

    protected function configure() {
        $this
            ->setName('init')
            ->setDescription('Initialize project inside current dir')
            ->setHelp("This command allows you create configuration inside current directory, which will become 
            project root");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $console = new SymfonyStyle($input, $output);

        if ($this->settings->exists()) {
            $console->error("Already initialized");
            return 1;
        }

        // additional automatic ignored paths
        $this->settings->version = new SemVer(VERSION);
        foreach (DEFAULT_IGNORE_LIST as $ignore)
            $this->settings->ignore[] = $ignore;

        $this->settings->server->host = $console->ask("Host");
        $this->settings->server->dir = $console->ask("Directory");
        $this->settings->server->username = $console->ask("Username");

        $passwordQuestion = new Question("Password (or leave empty)");
        $passwordQuestion->setHidden(true);
        $passwordQuestion->setMaxAttempts(null); // infinite seems not to work at all :/
        $passwordQuestion->setValidator(function($password) {
            // so what, we set the password even if its invalid, but we need to try to connect
            $this->settings->server->password = $password;
            if ($password != null && !$this->settings->server->connect()) {
                throw new InvalidArgumentException("connection/authentication failed");
            } else {
                return $password != null ? $password : null;
            }
        });

        do {
            $this->settings->server->password = $console->askQuestion($passwordQuestion);
        } while ($this->settings->server->password !== null && !$this->settings->server->connect());

        $this->settings->save();

        return 0;
    }
}