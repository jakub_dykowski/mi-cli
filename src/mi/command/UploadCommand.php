<?php
namespace mi\command;

use mi\App;
use mi\RemoteProject;
use mi\Settings;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class UploadCommand extends Command {

    private static $ARG_FILE = 'file';

    /** @var Settings */
    private $settings;

    /** @var callable */
    private $requireInitializedProject;

    /** @var string */
    private $projectDir;

    public function __construct(Settings $settings, callable $requireInitializedProject, $projectDir) {
        parent::__construct();
        $this->settings = $settings;
        $this->requireInitializedProject = $requireInitializedProject;
        $this->projectDir = $projectDir;
    }

    protected function configure() {
        $this
            ->setName('upload')
            ->setDescription('Upload selected files')
            ->setHelp("This command allows you upload selected files. It doesn't enter maintenance.")
            ->addArgument(
                self::$ARG_FILE,
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'File that you want to upload'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $console = new SymfonyStyle($input, $output);

        $dir = getcwd();
        /** @var SplFileInfo[] $filesToUpload */
        $filesToUpload = [];

        //sprawdź czy wszystkie pliki istnieją
        foreach ($input->getArgument(self::$ARG_FILE) as $filename) {
            $fileInfo = new SplFileInfo("$dir/$filename");
            if ($fileInfo->isFile()) {
                $filesToUpload[$filename] = $fileInfo;
            } else {
                $console->error("File doesn't exist: $filename");
                return 1;
            }
        }

        $requireInitializedProject = $this->requireInitializedProject;
        $requireInitializedProject();

        // jeśli nie zapisujemy hasła w configu, to zapytaj o nie użytkownika
        if ($this->settings->server->password == null) {
            $this->settings->server->password = $console->askHidden(
                "Password for {$this->settings->server->username}@{$this->settings->server->host}");
        }

        if (!$this->settings->server->connect()) {
            $console->error("connection to {$this->settings->server->getFullName()} failed");
            return 1;
        }

        $diffTitleShown = false;
        $diffTitle = "Production diff:";

        $production = new RemoteProject($this->settings->server, $this->settings->ignore);
        $localFs = App::getLocalFileSystemInfo($this->projectDir, $this->settings);
        $lock = $production->setLock(true);
        if (!$lock) {
            $console->error("Production locked by another process");
            return 1;
        }
        try {
            // display files that actually differ
            foreach ($filesToUpload as $path => $fileInfo) {
                if ($production->fileExists($path)) {
                    if ($production->getFileSystemInfo()->getChecksum($path) != $localFs->getChecksum($path)) {
                        if (!$diffTitleShown) {
                            $console->title($diffTitle);
                            $diffTitleShown = true;
                        }
                        $console->text("<fg=blue>M</> $path");
                    } else {
                        unset($filesToUpload[$path]);
                    }
                } else {
                    if (!$diffTitleShown) {
                        $console->title($diffTitle);
                        $diffTitleShown = true;
                    }
                    $console->text("<fg=green>A</> $path");
                }
            }

            if (count($filesToUpload) == 0) {
                $console->text("Everything up-to-date");
            } else {

                if ($console->ask("Are you sure to upload? (y/n)", "n") === "y") {
                    // lets upload
                    foreach ($filesToUpload as $path => $fileInfo) {
                        $production->sendFile($fileInfo->getPathname(), $path);
                    }

                    // verify checksums
                    $production->getFileSystemInfo()->clearCache(); // potrzebne by usunąć zcachowane sumy kontrolne
                    foreach ($filesToUpload as $path => $fileInfo) {
                        // we only check files, folders are sure to succeed
                        if ($fileInfo->isFile()) {
                            if ($production->getFileSystemInfo()->getChecksum($path) != $localFs->getChecksum($path)) {
                                $console->error("uploaded file checksum mismatch: $path");
                                return 2;
                            }
                        }
                    }
                } else {
                    $console->text("Cancelled");
                }
            }

        } finally {
            $production->setLock(false);
        }

        return 0;
    }

}