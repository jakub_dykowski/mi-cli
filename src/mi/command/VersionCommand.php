<?php
namespace mi\command;

use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class VersionCommand extends Command {

    /** @var string */
    private $version;

    public function __construct($version) {
        if (!is_string($version))
            throw new InvalidArgumentException("version must be a string");
        parent::__construct();
        $this->version = $version;
    }

    protected function configure() {
        $this
            ->setName('version')
            ->setDescription('Get the app version.')
            ->setHelp("This command allows you check current app version.");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        // just print the version
        $output->writeln($this->version);
    }

}