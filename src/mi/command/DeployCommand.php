<?php
namespace mi\command;

use mi\App;
use mi\RemoteProject;
use mi\Settings;
use mi\ShellUtils;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class DeployCommand extends Command {

    /** @var Settings */
    private $settings;

    /** @var callable */
    private $requireInitializedProject;

    /** @var string */
    private $projectDir;

    public function __construct(Settings $settings, callable $requireInitializedProject, $projectDir) {
        parent::__construct();
        $this->settings = $settings;
        $this->requireInitializedProject = $requireInitializedProject;
        $this->projectDir = $projectDir;
    }

    protected function configure() {
        $this
            ->setName('deploy')
            ->setDescription('Deploy project.')
            ->setHelp("This command allows you to deploy project to server.");
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $console = new SymfonyStyle($input, $output);

        App::debug("deploy...");
        $requireInitializedProject = $this->requireInitializedProject;
        $requireInitializedProject();

        // jeśli nie zapisujemy hasła w configu, to zapytaj o nie użytkownika
        if ($this->settings->server->password == null) {
            $this->settings->server->password = $console->askHidden(
                "Password for {$this->settings->server->username}@{$this->settings->server->host}");
        }

        $out = null;
        if (!$this->settings->server->connect()) {
            $console->error("connection to {$this->settings->server->getFullName()} failed");
            return 1;
        }

        $production = new RemoteProject($this->settings->server, $this->settings->ignore);
        $lock = $production->setLock(true);
        if (!$lock) {
            $console->error("Production locked by another process");
            return 1;
        }
        try {
            // complete differential upload
            App::debug("deploy diff...");

            $localFs = App::getLocalFileSystemInfo($this->projectDir, $this->settings);
            $productionFs = $production->getFileSystemInfo();
            $projectIterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($this->projectDir, RecursiveDirectoryIterator::SKIP_DOTS),
                RecursiveIteratorIterator::SELF_FIRST,
                RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
            );

            /** @var SplFileInfo[] $filesToUpload $path => SplFileInfo */
            $filesToUpload = [];
            $uploadSize = 0;
            $ignore = $this->settings->ignore;
            $ignore[] = basename($this->settings->getFile()); // chcemy pominąć także plik konfiguracyjny
            foreach (PREDEFINED_IGNORE_LIST as $fileToIgnore)
                $ignore[] = basename($fileToIgnore);

            $diffTitleShown = false;
            $diffTitle = "Production diff:";

            /**
             * Przejdź rekursywnie przez katalog projektu
             * @var string $iteratorRelativePath ścieżka relatywna do CWD z './' doklejonym na początku
             * @var SplFileInfo $fileInfo
             */
            foreach ($projectIterator as $iteratorRelativePath => $fileInfo) {
                if ($fileInfo->isLink())
                    continue; // skip links, uploading links seems to not work out of the box

                $path = App::getProjectRelativePath($fileInfo->getPathname(), $this->projectDir);
                // pomiń ignorowane
                foreach ($ignore as $ign)
                    if (strpos($path, $ign) === 0)
                        if ($path === $ign || substr($path, mb_strlen($ign), 1) === '/')
                            continue 2; // ignoruj jeśli zgadza się z $ign albo zaczyna się od $ign/
                $uploadCountBefore = count($filesToUpload);

                if ($fileInfo->isDir()) {
                    // dir
                    if (!$productionFs->dirExists($path)) {
                        // trzeba dodać folder by mógł się stworzyć po stronie serwera
                        $filesToUpload[$path] = $fileInfo;
                        if (!$diffTitleShown) {
                            $console->title($diffTitle);
                            $diffTitleShown = true;
                        }
                        $console->text("<fg=green>A</> $path");
                        App::debug("dir doesn't exist on production, need to add it: $fileInfo");
                    }
                } elseif ($fileInfo->isFile()) {
                    // file
                    if ($productionFs->fileExists($path)) {
                        if (!$production->isFileEqual($path, $localFs)) {
                            // changed
                            if (!$diffTitleShown) {
                                $console->title($diffTitle);
                                $diffTitleShown = true;
                            }
                            $console->text("<fg=blue>M</> $path");
                            $filesToUpload[$path] = $fileInfo;
                        }
                    } else {
                        // added
                        if (!$diffTitleShown) {
                            $console->title($diffTitle);
                            $diffTitleShown = true;
                        }
                        $console->text("<fg=green>A</> $path");
                        App::debug("file doesn't exist on production, need to add it: $fileInfo");
                        $filesToUpload[$path] = $fileInfo;
                    }

                    // policzmy wielkość uploadu
                    if (count($filesToUpload) > $uploadCountBefore)
                        $uploadSize += $fileInfo->getSize();
                } else {
                    $console->error("Unknown filesystem item type: {$fileInfo->getPathname()}");
                    return 1;
                }
            }

            if (count($filesToUpload)) {
                App::info("total upload size: %s", App::prettySize($uploadSize));
                App::info("total upload items count: %s", count($filesToUpload));
                // potwierdź deploy
//                if ($console->ask("Are you sure to deploy? (y/n)", 'n') == "y") {
                if ($console->ask("Are you sure to deploy? (y/n)", "n") === "y") {
                    // trzeba wprowadzić tryb maintenance
                    $production->setMaintenance(true);

                    // upload plików/folderów
                    $currentUpload = 0;
                    $progressBar = $console->createProgressBar(count($filesToUpload));
                    $progressBar->start();
                    $start = round(microtime(true) * 1000);
                    foreach ($filesToUpload as $path => $fileInfo) {
                        App::debug("syncing next file: %s", $fileInfo);
                        if ($fileInfo->isFile()) {
                            $success = $production->sendFile($fileInfo->getPathname(), $path);
                            if (!$success) {
                                $console->error("upload failed: $fileInfo");
                                return 1;
                            }
                            $currentUpload += $fileInfo->getSize();
                        } elseif ($fileInfo->isDir()) {
                            // tworzymy katalog
                            $production->exec("mkdir '$path'");
                        } else {
                            $console->error("Unknown filesystem item type: {$fileInfo->getPathname()}");
                            return 1;
                        }
                        $progressBar->advance();
                    }
                    $progressBar->finish();
                    $console->newLine(); // we need cursor to be below progressbar

                    // koniec uploadu
                    $stop = round(microtime(true) * 1000);
                    App::info("upload time: %s", $stop - $start);

                    // sprawdzenie poprawności
                    $productionFs->clearCache();
                    foreach ($filesToUpload as $path => $fileInfo) {
                        // sprawdzamy tylko pliki, foldery są pewne
                        if ($fileInfo->isFile()) {
                            if ($productionFs->getChecksum($path) != $localFs->getChecksum($path)) {
                                $console->error("uploaded file checksum mismatch: $path");
                                return 2;
                            }
                        }
                    }

                    // wyłączamy maintenance, tylko jeśli sumy kontrolne się zgadzają
                    $production->setMaintenance(false);
                } else {
                    $console->text("Cancelled");
                }
            } else {
                $console->text("Everything up-to-date");
            }
        } finally {
            $production->setLock(false);
        }

        return 0;
    }

}