<?php
namespace mi\command;

use mi\App;
use mi\RemoteProject;
use mi\Settings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 *
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 */
class MaintenanceCommand extends Command {

    private static $ARG_NEW_VALUE = "value";

    /** @var Settings */
    private $settings;

    /** @var callable */
    private $requireInitializedProject;

    public function __construct(Settings $settings, callable $requireInitializedProject) {
        parent::__construct();
        $this->settings = $settings;
        $this->requireInitializedProject = $requireInitializedProject;
    }

    protected function configure() {
        $this
            ->setName('maintenance')
            ->setDescription('Get or set maintenance mode')
            ->setHelp("This command allows you to check or set maintenance mode for configured server")
            ->addArgument(
                self::$ARG_NEW_VALUE,
                InputArgument::OPTIONAL,
                'true/false depending on whether you want to enter/exit maintenance'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $console = new SymfonyStyle($input, $output);
        $newValue = $input->getArgument(self::$ARG_NEW_VALUE);
        switch ($newValue) {
            case 'true':
            case 'false':
            case null:
                $requireInitializedProject = $this->requireInitializedProject;
                $requireInitializedProject();

                // jeśli nie zapisujemy hasła w configu, to zapytaj o nie użytkownika
                if ($this->settings->server->password == null) {
                    $this->settings->server->password = $console->askHidden(
                        "Password for {$this->settings->server->username}@{$this->settings->server->host}");
                }

                $out = null;
                if (!$this->settings->server->connect()) {
                    $output->writeln("connection to {$this->settings->server->getFullName()} failed");
                    return 1;
                }

                $production = new RemoteProject($this->settings->server, $this->settings->ignore);

                $lock = $production->setLock(true);
                if (!$lock) {
                    $output->writeln("Production locked by another process");
                    return 1;
                }
                try {
                    if ($newValue === null) {
                        // tylko pobierz aktualny status maintenance
                        App::println($production->isMaintenance() ? 'true' : 'false');
                    } else {
                        // ustaw status maintenance
                        $newMaintenance = $newValue === 'true';
                        if ($newMaintenance === $production->isMaintenance()) {
                            $output->writeln("Production maintenance status is already $newValue");
                            return 1;
                        }
                        $production->setMaintenance($newMaintenance);
                    }
                } finally {
                    $production->setLock(false);
                }
                break;
            default:
                throw new InvalidArgumentException("invalid 'value' argument");
        }
    }
}