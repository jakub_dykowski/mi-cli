<?php
/**
 * @author Jakub Dykowski <jakub.dykowski@gmail.com>
 * @license MIT
 */

use mi\App;
use mi\command\DeployCommand;
use mi\command\InitCommand;
use mi\command\MaintenanceCommand;
use mi\command\UploadCommand;
use mi\command\VersionCommand;
use mi\Settings;
use Symfony\Component\Console\Application;

const VERSION = "1.0.3";
const DEBUG = false;
const SCRIPT = __FILE__;
const CONFIG_FILE_NAME = "mi-cli.json";
const DEFAULT_IGNORE_LIST = ['.git', '.svn', '.idea'];
const MAINTENANCE_FILE_NAME = '.maintenance';
const PREDEFINED_IGNORE_LIST = [MAINTENANCE_FILE_NAME];

require "autoload.php";

$projectDir = App::discoverProjectRoot(CONFIG_FILE_NAME);
// jeśli projekt nie istnieje zakładamy że obecny katalog jest rootem by potem stworzyć w nim configa
$configDir = $projectDir !== null ? $projectDir : getcwd();
$settings = new Settings("$configDir/" . CONFIG_FILE_NAME);

$requireInitializedProject = function() use ($settings, $projectDir) {
    if ($projectDir === null) {
        App::println("You have to be inside an mi-cli project in order to use this command");
        exit(1);
    }

    // sprawdzamy na wszelki wypadek, ale projectDir już powinny wykryć czy config istnieje czy nie, jednakże
    // sprawdzamy czy ładowanie ustawień się powiodło
    if (!$settings->load()) {
        App::println("There were problem with loading config: %s", $settings->getFile());
        exit(1);
    }
};

$application = new Application("mi", VERSION);

// register commands
$application->add(new DeployCommand($settings, $requireInitializedProject, $projectDir));
$application->add(new UploadCommand($settings, $requireInitializedProject, $projectDir));
$application->add(new InitCommand($settings));
$application->add(new MaintenanceCommand($settings, $requireInitializedProject));
$application->add(new VersionCommand(VERSION));

$application->run();