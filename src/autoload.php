<?php
/*
 * Autoładowanie klas.
 * Zgodne z PSR-0
 */

spl_autoload_register(function($className) {
    $className = ltrim($className, '\\');
    $fileName = '';

    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    // najpierw spróbuj załadować plik projektu
    $projectFile = $fileName . str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    if (file_exists($projectFile)) {

        /** @noinspection PhpIncludeInspection */
        require $projectFile;
    }
}, true);