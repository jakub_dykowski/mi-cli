#!/usr/bin/env php
<?php
$srcDir = "src";
$buildDir = "build";
$libDir = "lib";

if (!file_exists($buildDir))
    mkdir($buildDir);

$phar = new Phar("$buildDir/mi-cli.phar",
    FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME,
    "mi-cli.phar");
$phar->buildFromDirectory($srcDir);
$phar->buildFromDirectory($libDir);
$phar->setStub($phar->createDefaultStub("cli.php"));